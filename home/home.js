/***********************************************************
 * Copyright (c) 2015 CyberGIS Center for Advanced Digital *
 * and Spatial Studies (CyberGIS). All Rights Reserved.    *
 ***********************************************************/
/**
 * home/home.js: Javascript file to define client-side helpers and event handlers
 * Author: Garrett Nickel <gmnicke2@illinois.edu>
 * Date: 08/20/2015
 */

Router.route('/', function () {
    this.render('Home');
});

Router.route('/manage/group', function() {
    this.render('manageGroup');
})

if (Meteor.isClient) {

    // Session variables to handle various scenarios
    Session.setDefault('fields_required', true);
    Session.setDefault('database_err', false);
    Session.setDefault('full_name', null);
    Session.setDefault('SSH_key', null);
    Session.setDefault('repo_curr', null);
    Session.setDefault('email_curr', null);
    Session.setDefault('logInLinux', null);
    Session.setDefault('groupName', null);
    Session.setDefault('phone_Num', null);

    document.title = 'OpenStack User Form';
    Template.submission.helpers({

        // Returns true if it's this user's first time registering
        // Used to make public key required or not
        required: function () {
            /*console.log('in required, session var is: '+Session.get('pubkey_required'));*/
            Meteor.call('fieldsRequired', function (error, result) {
                if (error) {
                    console.log('ERROR W/ fieldsRequired\n' + error);
                } else {
                    Session.set('fields_required', result);
                }
            });
            return Session.get('fields_required');
        },
        // Returns true if a database error occured on submit
        // Used to render an error message in the DOM
        databaseError: function () {
            return Session.get('database_err');
        },
        currentFullName: function () {
            Meteor.call('fullName', function (error, result) {
                if (!error) {
                    Session.set('full_name', result);
                }
            });
            return Session.get('full_name');
        },
        currrentSSHkey: function () {
            Meteor.call('sshKey', function (error, result) {
                if (!error) {
                    Session.set('SSH_key', result);
                }
            });
            return Session.get('SSH_key');
        },
        currentRepo: function () {
            Meteor.call('repo', function (error, result) {
                if (!error) {
                    Session.set('repo_curr', result);
                }
            });
            return Session.get('repo_curr');
        },
        currentEmail: function () {
            Meteor.call('emailTEMP', function (error, result) {
                if (!error) {
                    Session.set('email_curr', result);
                }
            });
            return Session.get('email_curr');
        },
        currentLuxName: function () {
            Meteor.call('luxUserName', function (error, result) {
                if (!error) {
                    Session.set('logInLinux', result);
                }
            });
            return Session.get('logInLinux');
        },
        currentPhoneNum: function () {
            Meteor.call('phoneNum', function (error, result) {
                if (!error) {
                    Session.set('phone_Num', result);
                }
            });
            return Session.get('phone_Num');
        },
        currentGroupName: function () {
            Meteor.call('groupNam', function (error, result) {
                if (!error) {
                    Session.set('groupName', result);
                }
            });
            return Session.get('groupName');
        }
    });

    Template.submission.events({
        // Event handler for submitting the user form
        "submit .submit-form": function (event, template) {

            // Prevent default form submit action
            //event.preventDefault();

            /* console.log(event);
             for(var i = 0; i < event.target.length - 1; i++) {
             console.log(event.target[i].value);
             }*/

            var full_name = event.target[0].value;
            var lux_name = event.target[1].value;
            var pub_key = event.target[2].value;
            var pref_repo = event.target[3].value;
            
            /**
             * change email so that users need to enter it twice
             * checkbox for sms or email notification
             * change the listing for fields
             * @type {string|string|Number}
             */
            var email_change = event.target[4].value;
            var phoneNumber = event.target[6].value;
            var email_conf_chang = event.target[5].value;

            if (email_change != email_conf_chang) {
                event.preventDefault();
                alert("Email does not match. Please try again");
            }

            //check lux_name if uniq
            Meteor.call("checkLinuxUniq", lux_name, function (error, result) {

                if (result) {

                    console.log("ohshit"+error);
                    event.preventDefault();
                }
                console.log("what");
            }),

                // Call server function to update/add to database
                Meteor.call("addToDatabase", full_name, pub_key,
                    pref_repo, email_change, lux_name, phoneNumber, function (error, result) {
                        if (error) {
                            console.log('ERROR W/ addToDatabase\n' + error);
                            Session.set('database_err', true);
                        } else {
                            Session.set('database_err', false);
                        }
                    });

            // Set form elements back to default values
            for (var i = 0; i < event.target.length - 1; i++) {
                event.target[i].value = "";
            }
        },
        'click button': function () {
            Meteor.call("sendemail");

        },
        'click a[target=_blank]': function (event) {
            event.preventDefault();
            //console.log(Meteor.userId());
            window.open(event.target.href + "users/" + Meteor.userId(), 'this_that');
        }
    });

    Template.groupSearch.helpers({
        groupsIndex: function() {
            return GroupsIndex;
        }
    });

    Template.userSearch.helpers({
        usersIndex: function() {
            return UsersIndex;
        }
    });

    Template.manageGroup.events({
        'submit .submit-users': function(event) {
            var groups = [];
            var users = [];

            var checkboxes = document.getElementsByName('groups_checkbox');
            for (var i=0; i<checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    groups.push(checkboxes[i].value);
                }
            }

            var checkboxes = document.getElementsByName('users_checkbox');
            for (var i=0; i<checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    users.push(checkboxes[i].value);
                }
            }

            groups.forEach(function(group) {
                Meteor.call("groupAdd", group, users, function(error, result) {
                    if(error) {
                        console.log("ERROR: Could not add users " + users + "to group " + group);
                    }
                });
            });
        }
    });

    Accounts.ui.config({
        passwordSignupFields: "USERNAME_AND_EMAIL"
    });

}


