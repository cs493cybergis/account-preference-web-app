Meteor client and server HTML/CSS/Javascript files and images, along with
meteor boilerplate, defining the OpenStack Preference Submission form

The client page consists of the create/log in tab and a web from where the user can input all the preferences.

The admin page will have to add /admin at the end of the url. Admins will be able to interact with the database via GUI.

Content:
	home/home.{html,css,js}
	public/bg.png
	public/submit_bg.gif
	public/submit_hover_bg.gif
	public/background.jpg
	server/routes.js
	server/server-methods.js
	server/smtp.js
	lib/app.js

# Running the App #

To launch the app clone the repo using `git clone git@bitbucket.org:cs493cybergis/account-preference-web-app.git`.

Then `cd` into the directory and run `meteor`.  If you want the app to run on port 80 you will need sudo access.  To launch the app on port 80 use the -p flag, so the command would be `sudo meteor -p 80`.  Finally to run the app in the background use nohup.  To launch the app in the background run `sudo nohup meteor -p 80 &`.


# Overview #


Once the app is running you can navigate to your ip address and begin to use the site.  There are three sections to the site, the user profile, admin, and group management.  The user profile is at the root of the site so that should be the first place you see.  To access the admin section add `/admin` to the end of the url.  You will need admin permissions to access this section of the site.  From the admin dashboard you can view/modify/create users and groups profile's. Finally navigate to `/manage/group` to access the group management.  From this page you can select multiple users to add to a group.

## Giving a User Admin Permissions ##

To give a user admin access navigate to the openstack_users collections from the left hand side of the admin dashboard.  Take note of their `user_id` and then click on the houston_admins link on the left hand side of the admin dashboard.  Then click the green `+ New houston_admins` and enter the user_id you received from the last page and enter `true` for the exists field. Finally click `+ Add` to give the user admin permissions.

## Adding a Group ##

To create a group simply navigate to the groups collection in the left hand side of the admin dashboard.  Then click the green `+ New groups` and enter an appropriate name and description.  Finally click `+ Add` to create the group.

## Adding Users to a Group in Bulk ##

Navigate to `/manage/group` and select the users that you want to add to a group.  You can use the search bar to filter users to refine your search.  Also, select the group you want to add them to and finally click the `Add Users to Group` button.

## Removing Users from a Group ##

Navigate to `/admin` and select the `openstack_users` collection.  Next, select the user you want to remove.  Then, remove the group name from their group field and press the `Save` button.

# API Documentation #

* GET `/users` - returns a list of all users
* GET `/users/<username>` - returns a specific user's data
* GET `/groups` - returns a list of all the groups
* GET `/groups/<groupname>` returns a specific group's data
* POST `/notify/<username>` - sends an email to the user with optional parameter `message`.  If no message parameter is provided the default "Your account is ready." message will be sent.