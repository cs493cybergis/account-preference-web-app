/***************************************************************
 * Copyright (c) 2015 CyberGIS Center for Advanced Digital     *
 * and Spatial Studies (CyberGIS). All Rights Reserved.        *
 ***************************************************************/
/**
 * server/routes.js:  Defines two routes as REST endpoints to GET all/one user(s)
 * Author: Garrett Nickel <gmnicke2@illinois.edu>
 * Date: 08/20/2015
 **

 /**
 * Provides formatting even if browser automatically format JSON
 */

Router.route('/notify/:_user', {where: 'server'})
    .post(function() {
    var res = this.response;
    var username = this.params._user;
    var user = UserInfoStore.find({"username": username}).fetch();
    var message;
    if(this.request.body.message) {
        message = this.request.body.message;
    } else {
        message = "Your account is ready.";
    }

    Email.send({
            from: 'no-reply@cybergis.ncsa.illinois.edu',
            to: user[0].email,
            subject: 'CyberGIS Notification',
            html: message
    });
    res.end("Successfully notified user " + username);
});

Router.route('/groups', function () {
    var res = this.response;
    res.end(JSON.stringify(Groups.find().fetch(), null, 2));
}, {where: 'server'});

Router.route('/groups/:_group/', function () {
    var group = this.params._group;
    var res = this.response;
    res.end(JSON.stringify(Groups.find({name: group}).fetch(), null, 2));
}, {where: 'server'});

Router.route('/users/', function () {
    /*
     Renders a stylized list of JSONs for user who have submitted a form
     */
    console.log(this.params.query);
    // console.log(Meteor.user());
    var opts = {};
    if (this.params.query.fullName) {
        opts.full_name = new RegExp('.*' + this.params.query.fullName + ".*", 'i');
    }
    if (this.params.query.userName) {
        opts.username = new RegExp('.*' + this.params.query.userName + ".*", 'i');
    }
    if (this.params.query.group) {
        var groups = this.params.query.group.split(",");
        var groupsRegExp = [];
        groups.forEach(function(elem) {
            groupsRegExp.push(new RegExp('.*' + elem + ".*", 'i'))
        })
        opts.groups = {
            $in: groupsRegExp
        }
    }

    var res = this.response;
    res.end(JSON.stringify(UserInfoStore.find(opts).fetch(), null, 2));
}, {where: 'server'});

Router.route('/users/:_user/', function () {
    /*
     Renders a stylized JSON for a specific user at /users/<user>
     */
    var user = this.params._user;
    var res = this.response;
    res.end(JSON.stringify(UserInfoStore.find({username: user}).fetch(), null, 2));

}, {where: 'server'});

