/******************************************************************
 * Copyright (c) 2015 CyberGIS Center for Advanced Digital and    *
 * Spatial Studies (CyberGIS). All Rights Reserved.               *
 ******************************************************************/
/**
 * server/server-methods.js: Defines server-side methods and email handling
 * Author: Garrett Nickel <gmnicke2@illinois.edu>
 * Date: 08/20/2015
 */

Houston.add_collection(Meteor.users);
Houston.add_collection(Houston._admins);
Houston.add_collection(Groups);
Houston.add_collection(UserInfoStore);

// To maintain the schema in Houston
if(Groups.find().count() === 0) {
    Groups.update({"_id": "A100"}, {"name": "group name", "description": "group description"}, {"upsert": true});
} else {
    Groups.remove({"_id": "A100"});
}

Meteor.methods({

    groupAdd: function (group, users) {
        if(Groups.find({name: group}).fetch().length !== 1) {
            console.log("The Group provided does not exist");
            return;
        }

        users.forEach(function(elem) {
            var user = UserInfoStore.find({"username": elem}).fetch();
            if(user[0] && user[0].groups.indexOf(group) === -1) {
                var groupList = user[0].groups;
                if(user[0].groups.length !== 0) {
                    groupList = groupList + ",";
                }
                UserInfoStore.update({"username": elem}, {$set: {groups: groupList+group}})
            } else {
                console.log("The user provided does not exist or is already a member of the group.");
                return;
            }
        });
    },
    groupNam: function () {
        // to be fixed
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var group_name = obj[0].groups;
        return group_name;
    },

    phoneNum: function () {
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var phoneNum = obj[0].phoneNum;
        return phoneNum;
    },

    luxUserName: function () {
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var luxname = obj[0].linuxUserName;
        return luxname;
    },

    emailTEMP: function () {
        return Meteor.user().emails[0].address;

    },

    fullName: function () {
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var fullName = obj[0].full_name;
        return fullName;
    },

    sshKey: function () {
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var publicKey = obj[0].public_key;
        return publicKey;
    },

    repo: function () {
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var repo = obj[0].pref_repo;
        return repo;
    },

    sendemail: function () {
        var user_id = Meteor.userId();
        var obj = UserInfoStore.find({user_id: user_id}).fetch();
        var fullName = obj[0].full_name;
        var publicKey = obj[0].public_key;
        var userName = obj[0].username;
        Email.send({
            from: 'no-reply@cybergis.ncsa.illinois.edu',
            to: Meteor.user().emails[0].address,
            subject: 'This is a test email',
            html: '<b>User Name: </b>' + JSON.stringify(userName) + '<br>' +
            '<b>Full name: </b>' + JSON.stringify(fullName) + '<br>' +
            '<b>Public Key: </b>' + JSON.stringify(publicKey) + '<br>' +
            '<b> User Name: </b>' + Meteor.user().username
        })
    },

    /*
     * Add the user's preferences to the database
     * Args: pub_key (string),
     pref_repo (string -- URL to preference repo)
     * Return: null
     */
    addToDatabase: function (full_name, pub_key, pref_repo, email_change, lux_name, phoneNum) {
        console.log("-----");
        console.log(Meteor.user());
        if (email_change) {
            console.log("Changing Email to " + email_change);
            Accounts.removeEmail(Meteor.userId(), Meteor.user().emails[0].address);
            Accounts.addEmail(Meteor.userId(), email_change);

        }
        console.log(Meteor.user().emails[0].address);

        if (!Meteor.userId()) {
            throw new Meteor.Error("You must be logged in to submit.");
        }

        //Meteor.user().emails[0].address = email_change;
        //Accounts.
        var user_id = Meteor.userId();
        // Only insert new entry if user ID not registered
        // Otherwise, update
        if (UserInfoStore.find({user_id: user_id}).count() === 0) {
            UserInfoStore.insert({
                full_name: full_name,
                public_key: pub_key,
                pref_repo: pref_repo,
                user_id: user_id,
                username: Meteor.user().username,
                email: Meteor.user().emails[0].address,
                linuxUserName: lux_name,
                phoneNum: phoneNum,
                groups: ""
            });
        } else {
            // Only update non-empty fields
            user = UserInfoStore.find({user_id: user_id}).fetch()[0]
            UserInfoStore.update(
                {user_id: user_id},
                {
                    $set: {
                        full_name: full_name.length > 0 ?
                            full_name :
                            user.full_name,
                        public_key: pub_key.length > 0 ?
                            pub_key :
                            user.public_key,
                        pref_repo: pref_repo.length > 0 ?
                            pref_repo :
                            user.pref_repo,
                        email: email_change.length > 0 ?
                            email_change :
                            user.email,
                        linuxUserName: lux_name.length > 0 ?
                            lux_name :
                            user.linuxUserName,
                        phoneNum: phoneNum.length > 0 ?
                            phoneNum :
                            user.phoneNum
                    }
                }
            );
        }
    },
    
    /*
     Checks if user has submitted a form before
     Args: none
     Returns:
     true if user has never submitted a form before
     false otherwise

     Purpose: Full name and SSH key required on first submit
     */
    fieldsRequired: function () {
        if (!Meteor.userId()) {
            return false;
        }

        var user_id = Meteor.userId();

        return UserInfoStore.find({user_id: user_id}).count() === 0;
    }
});

if (Meteor.isServer) {
    Meteor.startup(function () {
        /*
         * Define format & sender for verification email
         * E-mail code by Julien Le Coupanec at gentlenode.com
         * TODO: replace "from" email as well as 141.142... with actual values
         */
        Accounts.emailTemplates.from = 'no-reply <no-reply@cybergis.ncsa.illinois.edu>';

        Accounts.emailTemplates.siteName = 'OpenStack User Preference Form';

        Accounts.emailTemplates.verifyEmail.subject = function (user) {
            return 'Confirm Your Email for OpenStack User Preference Form';
        };

        Accounts.emailTemplates.verifyEmail.text = function (user, url) {
            var ipURL = url.replace("localhost", "141.142.168.56");
            return 'Click on the following link to verify your Email:\r\n' + ipURL; //url.replace("localhost","72.36.113.207")
        };

    });

    Accounts.onCreateUser(function (options, user) {
        user.profile = {};

        return user;
    });

    // Only allow user to login if their email is verified
    Accounts.validateLoginAttempt(function (attempt) {
        console.log(attempt.user);

        if (attempt.user && attempt.user.emails && !attempt.user.emails[0].verified) {
            console.log('Cannot log in - Email not verified');

            // Resends verification each login attempt w/o verification
            // A verification email is sent on user creation as well
            Meteor.setTimeout(function () {
                Accounts.sendVerificationEmail(attempt.user._id);
            }, 2000);
            throw new Meteor.Error(403, "Email not verified -- New Verification Email sent to " + attempt.user.emails[0].address);
            return false; // Login aborted
        }
        return true;
    });
}
