UserInfoStore = new Mongo.Collection('openstack_users');
Groups = new Mongo.Collection('groups');

UsersIndex = new EasySearch.Index({
    collection: UserInfoStore,
    fields: ['full_name', 'username', 'email'],
    engine: new EasySearch.MongoDB()
});

GroupsIndex = new EasySearch.Index({
	collection: Groups,
	fields: ['name'],
	engine: new EasySearch.MongoDB()
});